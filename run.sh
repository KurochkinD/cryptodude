./.heroku/python/bin/poetry config virtualenvs.create false
./.heroku/python/bin/poetry install --no-dev
./.heroku/python/bin/alembic upgrade head
./.heroku/python/bin/uvicorn app.main:app --host=0.0.0.0 --port=${PORT:-8000}
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.main import app
from app.routers.database import DATABASE_URL, Base, get_db
from app.routers.auth.db.models import User
from app.routers.auth.services import authenticate_user, create_access_token, get_current_user
from fastapi.exceptions import HTTPException

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_create_user():
    response = client.post(
        "/auth/register",
        json={"name": "abobus", "email": "deadpool@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "id" in data
    # get db to delete test user
    db = override_get_db().__next__()
    db.query(User).filter(User.email == data["email"]).delete()
    db.commit()


def test_bad_create_user():
    response = client.post(
        "/auth/register",
        json={"name": "abobus", "email": "amogusik", "password": "chimic"},
    )
    assert response.status_code == 400


def test_get_db():
    db = get_db().__next__()
    assert True


def test_token_auth():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    assert response.status_code == 200
    data = response.json()
    assert data["token_type"] == "bearer"

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_bad_token_auth():
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "nonexisting", "password": ",julfy"},
    )
    assert response.status_code == 401


def test_oauth():
    response = client.post("/auth/oauth")
    assert response.status_code == 307


def test_bad_password():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )

    db = override_get_db().__next__()
    res = authenticate_user(db, email="abobus@mail.ru", password="aboba")
    assert res is False

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_generate_token_timedelta():
    res = create_access_token(data={"username": "abobus", "password": "abobus"})
    assert type(res) == str


def test_get_current_user():
    db = override_get_db().__next__()

    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )

    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )

    token = response.json()["access_token"]

    res = get_current_user(
        token=token,
        db=db,
    )
    assert res.name == "abobus"

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_get_current_user_bad_token():
    db = override_get_db().__next__()
    try:
        res = get_current_user(token="abobus", db=db)
    except HTTPException:
        assert True


def test_logout():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )

    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )

    token = response.json()["access_token"]

    response = client.request(
        "post",
        "/auth/logout",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == 200

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_google_token():
    response = client.request(
        method="get",
        url="https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile&include_granted_scopes=true&response_type=token&redirect_uri=http://localhost:8000/auth/google_token&client_id=963035789345-2o8ubra9r5srkil2bac6s4jp9t6cvjqb.apps.googleusercontent.com",
        allow_redirects=True,
    )
    assert response.json()


def test_google_token_already_exists_user():
    response = client.get(
        "http://localhost:8000/auth/google_token?access_token=ya29.a0ARrdaM8b5wQKUracfFjORihb5mLVzPGFQrHm_4pEKq6NEXMozAV2gdp_OulJiuYKq4IZt8iQkhhBPu--3i7h3fAN01Tax3kbfIMRdRYqWi_dPesZ-g0T1XSIivEpng5n4n6kDUEqpBzpq8CgTWS96mPOCnBbPzc&token_type=Bearer&expires_in=3599&scope=email%20profile%20openid%20https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&authuser=0&prompt=none"
    )
    assert response.json()

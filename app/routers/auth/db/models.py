from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime


from app.routers.database import Base


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(String)
    photo = Column(String, default=None)
    register_time = Column(DateTime, default=datetime.utcnow())

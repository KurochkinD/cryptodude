from datetime import timedelta
import requests
import re

from fastapi import Depends, HTTPException, status, Request, responses, APIRouter
from fastapi.templating import Jinja2Templates
from fastapi.security import OAuth2PasswordRequestForm

from app.routers.constants import ACCESS_TOKEN_EXPIRE_MINUTES, CLIENT_ID
from app.routers.auth.schemas import Token, UserCreate, User
from app.routers.auth.services import get_current_user, authenticate_user, create_access_token
from app.routers.auth.db.services import get_user_by_email, create_user
from app.routers.database import engine, get_db
from app.routers.auth.db import models
from sqlalchemy.orm import Session

models.Base.metadata.create_all(bind=engine)

auth_app = APIRouter(prefix="/auth", tags=["Auth"])

templates = Jinja2Templates(directory="app/auth/templates")


@auth_app.post("/oauth")
async def oauth():
    """
    Endpoint will redirect to Google OAuth2 page to ask for credentials
    """
    return responses.RedirectResponse(
        f"https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com"
        f"/auth/userinfo.email "
        f"https://www.googleapis.com/auth/userinfo.profile&include_granted_scopes=true"
        f"&response_type=token&redirect_uri=http://localhost:8000/auth/google_token"
        f"&client_id={CLIENT_ID}"
    )


@auth_app.get("/google_token")
async def get_access_token(request: Request, db: Session = Depends(get_db)):
    """
    Endpoint will acquire information from user with Google OAuth2 and store it in DB
    """
    token = request.query_params.get("access_token")

    if token:
        response = requests.get(f"https://www.googleapis.com/oauth2/v3/userinfo?access_token={token}")
        userinfo = response.json()
        name = userinfo.get("name")
        email = userinfo.get("email")
        password = token[:16]
        user = UserCreate(name=name, email=email, password=password)
        if get_user_by_email(db, email) is None:
            create_user(db, user)

        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(data={"sub": user.email}, expires_delta=access_token_expires)
        request.session["token"] = access_token
        return {"access_token": access_token, "token_type": "bearer"}
    return status.HTTP_400_BAD_REQUEST


@auth_app.post("/token", response_model=Token)
async def login_for_access_token(
    request: Request, form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)
):
    """
    Login endpoint. Data must be send with multipart/form-data header.
    """
    user = authenticate_user(db=db, email=form_data.username, password=form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data={"sub": user.email}, expires_delta=access_token_expires)
    request.session["token"] = access_token
    return {"access_token": access_token, "token_type": "bearer"}


@auth_app.post("/register", response_model=User)
def register_user(user: UserCreate, db: Session = Depends(get_db)):
    """
    Registration endpoint. Content can be sent as a json in body.
    """
    db_user = get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    # should contain uppercase, lowercase, numbers, special symbols
    if not re.fullmatch(r'[A-Za-z0-9!@#$%^&*()_\-+={\[}\]|\\:;"\'<,>.?]{8,}', user.password):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Password should contain uppercase, lowercase letters, numbers and special symbols",
        )
    return create_user(db, user)


@auth_app.post("/logout")
def logout(request: Request, current_user: User = Depends(get_current_user)):
    """
    Logout endpoint. Clears session cookie so front-end cant take token anymore.
    """
    request.session.clear()
    return status.HTTP_200_OK

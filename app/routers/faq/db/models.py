from sqlalchemy import Column, Integer, String

from app.routers.database import Base


class FAQuestion(Base):
    __tablename__ = "faquestion"

    id = Column(Integer, primary_key=True)
    question = Column(String)
    answer = Column(String)

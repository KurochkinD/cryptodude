from typing import List, Union
from sqlalchemy.orm import Session

from app.routers.faq.db.models import FAQuestion


def serialize_faquestion(faquestion: FAQuestion) -> dict:
    return {"id": faquestion.id, "question": faquestion.question, "answer": faquestion.answer}


def query_all_faquestions(db: Session) -> List[dict]:
    result = db.query(FAQuestion).all()
    return [serialize_faquestion(faquestion) for faquestion in result]


def query_faquestion(db: Session, faq_id: int) -> Union[dict, bool]:
    faquestion = db.query(FAQuestion).filter(FAQuestion.id == faq_id).first()
    if faquestion is None:
        return False
    return serialize_faquestion(faquestion)

from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app.routers.database import get_db
from app.routers.faq.db.services import query_all_faquestions, query_faquestion
from app.routers.faq.schemas import FAQuestion

faq_app = APIRouter(prefix="/faq", tags=["FAQ"])


@faq_app.get("/", response_model=List[FAQuestion])
async def get_all_faquestions(db: Session = Depends(get_db)) -> List:
    """
    This endpoint will return the list with all available frequently asked questions
    Each question is represented by dict with the following keys: id, question, answer
    """
    return query_all_faquestions(db)


@faq_app.get("/{faq_id}", response_model=FAQuestion)
async def get_faquestion(db: Session = Depends(get_db), faq_id=int) -> dict:
    """
    This endpoint will return one question which is represented by dict with the following keys: id, question, answer
    :param faq_id: unique id of the question
    """
    response = query_faquestion(db, faq_id)
    if not response:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return response

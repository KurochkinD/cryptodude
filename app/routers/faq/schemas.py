from pydantic import BaseModel


class FAQuestion(BaseModel):
    id: int
    question: str
    answer: str

import asyncio

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from app.main import app
from app.routers.auth.db.models import User
from app.routers.database import DATABASE_URL, Base, get_db
from app.routers.exchanges.db.models import ExchangeSub, Exchange
from app.routers.exchanges.services import update_commissions, update_exchange_list

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


# def test_update_exchanges():
#     asyncio.run(update_exchange_list())
#     assert True


def test_update_commissions():
    asyncio.run(update_commissions())
    assert True


def test_get_all_exchange_info():
    exch = Exchange(name="test", url="test", logo=None)
    db = override_get_db().__next__()
    db.add(exch)
    db.commit()

    response = client.get("/exchanges/")
    assert response.status_code == 200
    data: list = response.json()
    assert len(data) > 0

    db.query(Exchange).filter(Exchange.name == "test").delete()
    db.commit()


def test_get_exchange_info():
    exch = Exchange(name="test", url="test", logo=None)
    db = override_get_db().__next__()
    db.add(exch)
    db.commit()
    exch_id = db.query(Exchange).filter(Exchange.name == "test").first().id

    response = client.get(f"/exchanges/{exch_id}")
    assert response.status_code == 200
    data: dict = response.json()
    assert data["id"] > 0

    db.query(Exchange).filter(Exchange.name == "test").delete()
    db.commit()


def test_user_sub_exchange_already_exists():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id

    client.post("/exchanges/270/subscribe", json={"user_id": user_id})
    response = client.post("/exchanges/270/subscribe", json={"user_id": user_id})
    assert response.status_code == 400

    db.query(ExchangeSub).filter(ExchangeSub.exchange_id == 270, ExchangeSub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_sub_exchange_success():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id

    response = client.post("/exchanges/270/subscribe", json={"user_id": user_id})
    assert response.status_code == 200

    db.query(ExchangeSub).filter(ExchangeSub.user_id == user_id, ExchangeSub.exchange_id == 270).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()

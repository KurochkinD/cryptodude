from datetime import datetime

from pydantic import BaseModel


class Exchange(BaseModel):
    id: int
    name: str
    url: str = None
    logo: str = None
    register_time: datetime = None
    maker_fee: float = None
    taker_fee: float = None
    fee_url: str = None


class UserID(BaseModel):
    user_id: int

from typing import List, Dict

from sqlalchemy.orm import Session

from app.routers.database import SessionLocal
from app.routers.exchanges.db.models import Exchange, ExchangeSub, Commission


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_all_exchanges_info_bd(db: Session) -> List[Dict]:
    result = db.query(Exchange).all()
    return [
        {
            "id": exchange.id,
            "name": exchange.name,
            "url": exchange.url,
            "logo": exchange.logo,
            "register_time": exchange.register_time,
            "maker_fee": exchange.maker_fee,
            "taker_fee": exchange.taker_fee,
            "fee_url": exchange.fee_url,
        }
        for exchange in result
    ]


def get_exchange_info(db: Session, exchange_id: int) -> Dict:
    exchange = db.query(Exchange).filter(Exchange.id == exchange_id).first()
    if exchange is not None:
        return {
            "id": exchange.id,
            "name": exchange.name,
            "url": exchange.url,
            "logo": exchange.logo,
            "register_time": exchange.register_time,
            "maker_fee": exchange.maker_fee,
            "taker_fee": exchange.taker_fee,
            "fee_url": exchange.fee_url,
        }
    else:
        return False


def create_subscription(db: Session, user_id: int, exchange_id: int) -> bool:
    sub = db.query(ExchangeSub).filter(ExchangeSub.user_id == user_id, ExchangeSub.exchange_id == exchange_id).first()
    if sub is None:
        sub_entry = ExchangeSub(user_id=user_id, exchange_id=exchange_id)
        db.add(sub_entry)
        db.commit()
        db.refresh(sub_entry)
        return True
    return False


def create_commission(db: Session, exchange_id: int, maker_fee: float, taker_fee: float, fee_url: str) -> None:
    new_commission = Commission(exchange_id=exchange_id, maker_fee=maker_fee, taker_fee=taker_fee, fee_url=fee_url)
    db.add(new_commission)
    db.commit()


def get_exchange_id_by_name(db: Session, exchange_name: str) -> int:
    exchange = db.query(Exchange).filter(Exchange.name == exchange_name).first()
    return exchange.id


def get_all_exchanges_id(db: Session) -> List[int]:
    exchanges = db.query(Exchange).all()
    return [exchange.id for exchange in exchanges]


def update_exchange_commission(db: Session) -> None:
    exchanges_dicts = [{"id": int(exchange.id)} for exchange in db.query(Exchange).all()]
    for exchange_dict in exchanges_dicts:
        new_commission = db.query(Commission).filter(Commission.exchange_id == int(exchange_dict["id"])).first()
        exchange_dict["commission_id"] = int(new_commission.id)
        exchange_dict["maker_fee"] = new_commission.maker_fee
        exchange_dict["taker_fee"] = new_commission.taker_fee
        exchange_dict["fee_url"] = new_commission.fee_url

    db.bulk_update_mappings(Exchange, exchanges_dicts)
    db.commit()


def create_exchange_info(db: Session, exchange_id: int, exchange_name: str, exchange_url: str, exchange_logo: str) -> None:
    new_exchange = Exchange(id=exchange_id, name=exchange_name, url=exchange_url, logo=exchange_logo)
    db.add(new_exchange)
    db.commit()

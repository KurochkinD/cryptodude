from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Float

from app.routers.database import Base


class Exchange(Base):
    __tablename__ = "exchange"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True)
    url = Column(String, unique=True)
    logo = Column(String, default=None)
    register_time = Column(DateTime, default=datetime.utcnow())
    commission_id = Column(Integer, ForeignKey("commission.id"), default=None, unique=True)
    maker_fee = Column(Float(precision=4), default=None)
    taker_fee = Column(Float(precision=4), default=None)
    fee_url = Column(String, default=None)


class Commission(Base):
    __tablename__ = "commission"

    id = Column(Integer, primary_key=True, index=True)
    exchange_id = Column(Integer, ForeignKey("exchange.id"), nullable=False, index=True)
    time = Column(DateTime, default=datetime.utcnow())
    maker_fee = Column(Float(precision=4), default=None)
    taker_fee = Column(Float(precision=4), default=None)
    fee_url = Column(String, default=None, unique=True)


class ExchangeSub(Base):
    __tablename__ = "exchange_sub"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False, index=True)
    exchange_id = Column(Integer, ForeignKey("exchange.id"), nullable=False)
    sub_time = Column(DateTime, default=datetime.utcnow())

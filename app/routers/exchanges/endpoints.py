from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.routers.exchanges.db.services import (
    get_db,
    get_all_exchanges_info_bd,
    get_exchange_info,
    create_subscription,
)
from app.routers.exchanges.schemas import Exchange, UserID

exchanges_app = APIRouter(prefix="/exchanges", tags=["Exchanges"])


@exchanges_app.get("/", response_model=List[Exchange])
async def get_all_exchanges_info(db: Session = Depends(get_db)):
    return get_all_exchanges_info_bd(db)


@exchanges_app.get("/{exchange_id}", response_model=Exchange)
async def get_exchanges_info(exchange_id: int, db: Session = Depends(get_db)):
    response = get_exchange_info(db, exchange_id)
    if not response:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return response


@exchanges_app.post("/{exchange_id}/subscribe")
async def subscribe_user_to_cryptocurrency(user_id: UserID, exchange_id: int, db: Session = Depends(get_db)):
    response = create_subscription(db, user_id.user_id, exchange_id)
    if not response:
        raise HTTPException(status_code=400, detail="Bad Request")
    else:
        return status.HTTP_200_OK

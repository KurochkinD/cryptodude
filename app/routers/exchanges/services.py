from typing import Tuple
import os
import aiohttp
import aiofiles
import asyncio
from app.routers.constants import COINMARKETCAP_KEY, EXCHANGES_LIST, STORAGE_PATH
from app.routers.database import get_db
from app.routers.exchanges.db.services import get_all_exchanges_id, update_exchange_commission, create_commission, create_exchange_info


async def get_exchange_commissions(exchange_id: int) -> Tuple[float, float, str]:
    async with aiohttp.ClientSession(
        headers={"X-CMC_PRO_API_KEY": COINMARKETCAP_KEY, "Accept": "application/json"}
    ) as session:
        url = f"https://pro-api.coinmarketcap.com/v1/exchange/info?id={exchange_id}"
        async with session.get(url) as response:
            info = await response.json()
            return (
                info["data"][str(exchange_id)]["maker_fee"],
                info["data"][str(exchange_id)]["taker_fee"],
                *info["data"][str(exchange_id)]["urls"]["fee"],
            )


async def update_commissions() -> None:
    db = get_db().__next__()
    exchanges_list = get_all_exchanges_id(db=db)

    for exchange_id in exchanges_list:
        commission_data = await get_exchange_commissions(exchange_id)
        create_commission(
            db=db,
            exchange_id=exchange_id,
            maker_fee=commission_data[0],
            taker_fee=commission_data[1],
            fee_url=commission_data[2],
        )
    update_exchange_commission(db)


async def get_exchanges_logo(exchange_id: int, size: int = 128) -> str:
    if size not in (64, 128):
        size = 128

    async with aiohttp.ClientSession(headers={"Accept": "application/json"}) as session:
        url = f"https://s2.coinmarketcap.com/static/img/exchanges/{size}x{size}/{exchange_id}.png"
        async with session.get(url) as response:
            path = os.path.join(STORAGE_PATH, f"exchanges/logos/{exchange_id}.png")
            file = await aiofiles.open(path, mode="wb")
            await file.write(await response.read())
            await file.close()
            return path


async def update_exchange_list() -> None:
    db = get_db().__next__()
    exchanges_list = get_all_exchanges_id(db=db)

    async with aiohttp.ClientSession(
        headers={"X-CMC_PRO_API_KEY": COINMARKETCAP_KEY, "Accept": "application/json"}
    ) as session:
        url_map = f"https://pro-api.coinmarketcap.com/v1/exchange/map"
        async with session.get(url_map) as map_response:
            exchange_map = await map_response.json()
            for map_plot in exchange_map["data"]:

                if map_plot["name"] in EXCHANGES_LIST and map_plot["id"] not in exchanges_list:
                    exchange_id = map_plot["id"]
                    url = f"https://pro-api.coinmarketcap.com/v1/exchange/info?id={exchange_id}"
                    async with session.get(url) as response:
                        info_resp = await response.json()
                        info = info_resp["data"][f"{exchange_id}"]
                        logo_path = await get_exchanges_logo(exchange_id)
                        create_exchange_info(db=db, exchange_id=exchange_id, exchange_name=info["name"], exchange_url=info["urls"]["website"], exchange_logo=logo_path)



# testing
if __name__ == "__main__":
    print(asyncio.run(update_exchange_list()))
    asyncio.run(update_commissions())

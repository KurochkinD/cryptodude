from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey


from app.routers.database import Base


class Currency(Base):
    __tablename__ = "currency"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    code_name = Column(String, unique=True)
    logo = Column(String, default=None)


class CurrencySub(Base):
    __tablename__ = "currency_sub"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False, index=True)
    currency_id = Column(Integer, ForeignKey("currency.id"), nullable=False)
    sub_time = Column(DateTime, default=datetime.utcnow())

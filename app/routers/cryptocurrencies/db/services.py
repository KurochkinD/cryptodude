from sqlalchemy.orm import Session

from app.routers.cryptocurrencies.db.models import Currency, CurrencySub


def query_all_crypto_info(db: Session):
    result = db.query(Currency).all()
    return [
        {"id": crypto.id, "name": crypto.name, "code_name": crypto.code_name, "logo": crypto.logo} for crypto in result
    ]


def query_crypto_info(db: Session, crypto_id: int):
    crypto = db.query(Currency).filter(Currency.id == crypto_id).first()
    if crypto is not None:
        return {"id": crypto.id, "name": crypto.name, "code_name": crypto.code_name, "logo": crypto.logo}
    else:
        return False


def query_create_subscription(db: Session, user_id: int, crypto_id: int):
    sub = db.query(CurrencySub).filter(CurrencySub.user_id == user_id, CurrencySub.currency_id == crypto_id).first()
    if sub is None:
        sub_entry = CurrencySub(user_id=user_id, currency_id=crypto_id)
        db.add(sub_entry)
        db.commit()
        db.refresh(sub_entry)
        return True
    else:
        return False


def create_crypto(db: Session, crypto_id: int, name: str, code_name: str, logo: str = None):
    new_cryptocurrency = Currency(id=crypto_id, name=name, code_name=code_name, logo=logo)

    db.add(new_cryptocurrency)
    db.commit()

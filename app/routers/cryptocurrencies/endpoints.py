from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.routers.cryptocurrencies.db.services import (
    query_all_crypto_info,
    query_crypto_info,
    query_create_subscription,
)
from app.routers.cryptocurrencies.schemas import CryptoCurrency, UserID
from app.routers.database import get_db

crypto_app = APIRouter(prefix="/crypto", tags=["Crypto"])


@crypto_app.get("/", response_model=List[CryptoCurrency])
async def get_all_crypto_info(db: Session = Depends(get_db)):
    return query_all_crypto_info(db)


@crypto_app.get("/{crypto_id}", response_model=CryptoCurrency)
async def get_crypto_info(crypto_id: int, db: Session = Depends(get_db)):
    response = query_crypto_info(db, crypto_id)
    if not response:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return response


@crypto_app.post("/{crypto_id}/subscribe")
async def subscribe_user_to_cryptocurrency(user_id: UserID, crypto_id: int, db: Session = Depends(get_db)):
    response = query_create_subscription(db, user_id.user_id, crypto_id)
    if not response:
        raise HTTPException(status_code=400, detail="Bad Request")
    else:
        return status.HTTP_200_OK

from pydantic import BaseModel


class CryptoCurrency(BaseModel):
    id: int
    name: str
    code_name: str
    logo: str = None


class UserID(BaseModel):
    user_id: int

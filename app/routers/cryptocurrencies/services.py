import os
from typing import List, Dict

import aiofiles
import aiohttp
import asyncio
from app.routers.constants import COINMARKETCAP_KEY, AMOUNT_OF_CRYPTOCURRENCIES
from app.routers.cryptocurrencies.db.services import query_crypto_info, create_crypto
from app.routers.database import get_db


STORAGE_PATH = os.path.abspath(os.path.join("app/", "storage/"))


async def get_cryptocurrencies_list() -> List[Dict]:
    async with aiohttp.ClientSession(
        headers={"X-CMC_PRO_API_KEY": COINMARKETCAP_KEY, "Accept": "application/json"}
    ) as session:
        url = f"https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"
        async with session.get(url) as response:
            info = await response.json()
            return info["data"][:AMOUNT_OF_CRYPTOCURRENCIES]


async def get_cryptocurrencies_logo(crypto_id: int, size: int = 128) -> str:
    if not (size == 64 or size == 128):
        size = 128
    async with aiohttp.ClientSession(headers={"Accept": "application/json"}) as session:
        url = f"https://s2.coinmarketcap.com/static/img/coins/{size}x{size}/{crypto_id}.png"
        async with session.get(url) as response:
            path = os.path.join(STORAGE_PATH, f"cryptocurrencies/logos/{crypto_id}.png")
            file = await aiofiles.open(path, mode="wb")
            await file.write(await response.read())
            await file.close()
            return path


async def update_cryptocurrencies() -> None:
    db = get_db().__next__()
    cryptocurrencies_list = await get_cryptocurrencies_list()

    for cryptocurrency in cryptocurrencies_list:
        if not query_crypto_info(db=db, crypto_id=cryptocurrency["id"]):
            logo_path = await get_cryptocurrencies_logo(crypto_id=cryptocurrency["id"])
            create_crypto(
                db=db,
                crypto_id=cryptocurrency["id"],
                name=cryptocurrency["name"],
                code_name=cryptocurrency["symbol"],
                logo=logo_path,
            )


if __name__ == "__main__":
    asyncio.run(update_cryptocurrencies())

import asyncio

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from app.main import app
from app.routers.auth.db.models import User
from app.routers.cryptocurrencies.db.models import CurrencySub
from app.routers.cryptocurrencies.services import update_cryptocurrencies
from app.routers.database import DATABASE_URL, Base, get_db

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_get_all_crypto_info():
    response = client.get("/crypto/")
    assert response.status_code == 200
    data: list = response.json()
    assert len(data) > 0


def test_get_crypto_info():
    response = client.get("/crypto/1")
    assert response.status_code == 200
    data: dict = response.json()
    assert data["id"] > 0


def test_user_sub_crypto_already_exists():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id

    client.post("/crypto/1/subscribe", json={"user_id": user_id})
    response = client.post("/crypto/1/subscribe", json={"user_id": user_id})
    assert response.status_code == 400

    db.query(CurrencySub).filter(CurrencySub.currency_id == 1, CurrencySub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_sub_crypto_success():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id

    response = client.post("/crypto/1/subscribe", json={"user_id": user_id})
    assert response.status_code == 200

    db.query(CurrencySub).filter(CurrencySub.user_id == user_id, CurrencySub.currency_id == 1).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_update_cryptocurrencies():
    asyncio.run(update_cryptocurrencies())
    assert True

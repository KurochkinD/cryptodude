import asyncio
from typing import List

from fastapi import APIRouter, Depends, WebSocket, Request
from sqlalchemy.orm import Session

from app.routers.crypto_prices.db.services import get_list_of_hot_prices
from app.routers.crypto_prices.schemas import PriceHistory
from app.routers.crypto_prices.services import get_list_of_price_histories
from app.routers.database import get_db

crypto_api = APIRouter(prefix="/crypto_api", tags=["CryptoAPI"])


@crypto_api.websocket("/hot_prices")
async def get_hot_prices(websocket: WebSocket, db: Session = Depends(get_db)):
    """
    This endpoint will return list of currencies with prices that changed significantly on market
    """
    await websocket.accept()
    while True:
        list_of_hot_prices = get_list_of_hot_prices(db)
        await websocket.send_json(list_of_hot_prices)
        await asyncio.sleep(300)


@crypto_api.get("/price_history", response_model=List[PriceHistory])
async def get_price_history(
    exchange: str, currency: str, start_time: str, end_time: str, db: Session = Depends(get_db)
):
    """
    This endpoint will return a list of currencies with prices on the specified exchange for a certain period of time
    """
    result = get_list_of_price_histories(db, exchange, currency, start_time, end_time)
    return result

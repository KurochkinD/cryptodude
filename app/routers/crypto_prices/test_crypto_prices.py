import asyncio
import datetime

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from app.main import app
from app.routers.crypto_prices.db.models import Rate
from app.routers.crypto_prices.db.services import get_list_of_hot_prices
from app.routers.cryptocurrencies.services import update_cryptocurrencies
from app.routers.database import get_db, DATABASE_URL
from app.routers.crypto_prices.services import update_currency_prices
from app.routers.exchanges.services import update_exchange_list

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_currency_updating():
    asyncio.run(update_exchange_list())
    asyncio.run(update_cryptocurrencies())
    db: Session = get_db().__next__()
    start_amount = len(db.query(Rate).all())

    asyncio.run(update_currency_prices())

    end_amount = len(db.query(Rate).all())
    assert end_amount - start_amount > 0


def test_get_list_of_hot_prices():
    asyncio.run(update_currency_prices())
    db: Session = get_db().__next__()
    data = get_list_of_hot_prices(db)

    assert len(data) > 0


def test_get_price_history():
    response = client.request(
        "get",
        "/crypto_api/price_history",
        params={
            "exchange": "all",
            "currency": "all",
            "start_time": "2022-01-01 22:17:17.336046",
            "end_time": str(datetime.datetime.utcnow()),
        },
    )

    res = response.json()
    assert len(res) > 0


def test_get_price_history_one_to_one():
    response = client.request(
        "get",
        "/crypto_api/price_history",
        params={
            "exchange": "Coinbase Exchange",
            "currency": "BTC",
            "start_time": "2022-01-01 22:17:17.336046",
            "end_time": str(datetime.datetime.utcnow()),
        },
    )

    res = response.json()
    assert len(res) > 0


def test_get_price_history_one_to_all():
    response = client.request(
        "get",
        "/crypto_api/price_history",
        params={
            "exchange": "Coinbase Exchange",
            "currency": "all",
            "start_time": "2022-01-01 22:17:17.336046",
            "end_time": str(datetime.datetime.utcnow()),
        },
    )

    res = response.json()
    assert len(res) > 0


def test_get_price_history_all_to_one():
    response = client.request(
        "get",
        "/crypto_api/price_history",
        params={
            "exchange": "all",
            "currency": "BTC",
            "start_time": "2022-01-01 22:17:17.336046",
            "end_time": str(datetime.datetime.utcnow()),
        },
    )

    res = response.json()
    assert len(res) > 0

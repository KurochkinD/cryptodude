from typing import List

from pydantic import BaseModel


class DatePrice(BaseModel):
    date: str
    price: float = None


class PriceHistory(BaseModel):
    exchange: str
    currency: str
    price_history: List[DatePrice]

from datetime import datetime

from sqlalchemy import Column, Integer, DateTime, ForeignKey, Float

from app.routers.database import Base


class Rate(Base):
    __tablename__ = "rate"

    id = Column(Integer, primary_key=True)
    time = Column(DateTime, default=datetime.utcnow())
    sell_currency_id = Column(Integer, ForeignKey("currency.id"), nullable=False)
    buy_currency_id = Column(Integer, ForeignKey("currency.id"), nullable=False)
    market_id = Column(Integer, ForeignKey("exchange.id"), nullable=False)
    price = Column(Float)

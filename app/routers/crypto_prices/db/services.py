from datetime import datetime
from typing import List

from sqlalchemy.orm import Session, load_only

from app.routers.crypto_prices.db.models import Rate
from app.routers.cryptocurrencies.db.models import Currency
from app.routers.database import get_db
from app.routers.exchanges.db.models import Exchange


def get_name_tags_of_currencies(db: Session) -> List[dict]:
    result = db.query(Currency).filter(Currency.code_name != "USDT").all()
    return [{"id": curr.id, "code_name": curr.code_name} for curr in result]


def get_info_of_exchanges(db: Session) -> List[dict]:
    result = db.query(Exchange).options(load_only("id", "name")).all()
    return [{"id": exchange.id, "name": exchange.name} for exchange in result]


def insert_new_prices(db: Session, currency_price_data: dict):
    USDT_ID = 825  # currency that we are selling to buy crypto by default
    to_insert = []
    for exchange, data in currency_price_data.items():
        market_id = db.query(Exchange).filter(Exchange.name == exchange).first().id
        for id_, price in data.items():
            to_insert.append(Rate(sell_currency_id=USDT_ID, buy_currency_id=id_, market_id=market_id, price=price))

    db.bulk_save_objects(to_insert)
    db.commit()


def get_list_of_hot_prices(db: Session):
    exchange_id_and_names = get_info_of_exchanges(db)
    exchange_ids = {exchange["id"]: exchange["name"] for exchange in exchange_id_and_names}
    currency_id_and_tags = get_name_tags_of_currencies(db)
    currency_ids = {curr["id"]: curr["code_name"] for curr in currency_id_and_tags}

    result = []
    for exchange_id in exchange_ids:
        for currency_id in currency_ids.keys():
            # запросить последние 2 цены криптовалюты (за сегодня и вчера)
            two_day_prices = (
                db.query(Rate)
                .filter(Rate.market_id == exchange_id, Rate.buy_currency_id == currency_id)
                .order_by(Rate.time.desc())  # пока заглушка, полагаем что раз в день запрашиваем
                .limit(2)
                .all()
            )

            if two_day_prices[0].price is None or two_day_prices[1].price == 0:
                continue
            change = 1 - two_day_prices[0].price / two_day_prices[1].price
            result.append(
                {
                    "exchange": exchange_ids[exchange_id],
                    "name_tag": currency_ids[currency_id],
                    "price": two_day_prices[1].price,
                    "change": change,
                }
            )

    result = sorted(result, key=lambda x: abs(x["change"]), reverse=True)
    return result


def get_price_history_one_to_one(
    db: Session, exchange: str, currency: str, start_time: str, end_time: str
) -> List[dict]:
    exchange_id = db.query(Exchange).filter(Exchange.name == exchange).first().id
    currency_id = db.query(Currency).filter(Currency.code_name == currency).first().id
    # 2014-03-12T13:37:27+00:00 - example start_time
    start_time_dt = datetime.fromisoformat(start_time)
    end_time_dt = datetime.fromisoformat(end_time)
    price_history = (
        db.query(Rate)
        .filter(
            Rate.market_id == exchange_id,
            Rate.buy_currency_id == currency_id,
            Rate.time >= start_time_dt,
            Rate.time <= end_time_dt,
        )
        .all()
    )

    if len(price_history) > 20:
        step = len(price_history) // 20
        price_history = price_history[::step]

    price_history = [
        {
            "exchange": exchange,
            "currency": currency,
            "price_history": [{"date": str(rate.time), "price": rate.price} for rate in price_history],
        }
    ]
    return price_history


def get_price_history_one_to_all(db: Session, exchange: str, start_time: str, end_time: str) -> List[dict]:
    exchange_id = db.query(Exchange).filter(Exchange.name == exchange).first().id
    # 2014-03-12T13:37:27+00:00 - example start_time
    start_time_dt = datetime.fromisoformat(start_time)
    end_time_dt = datetime.fromisoformat(end_time)
    price_history = (
        db.query(Rate, Currency)
        .join(Currency, Rate.buy_currency_id == Currency.id)
        .filter(
            Rate.market_id == exchange_id,
            Rate.time >= start_time_dt,
            Rate.time <= end_time_dt,
        )
        .order_by(Currency.name)
        .all()
    )

    if len(price_history) > 100:
        step = len(price_history) // 20
        price_history = price_history[::step]

    # take all distinct currency names, then for each name iterate through price_history to find records with that exact
    # name and add date and price of that records to dict of price history for that currency, then add data about
    # currency to result dict
    result = []
    for currency in set([rate_and_currency[1].name for rate_and_currency in price_history]):
        curr_price_history = []
        for rate_and_currency in price_history:
            if rate_and_currency[1].name == currency:
                curr_price_history.append({"date": str(rate_and_currency[0].time), "price": rate_and_currency[0].price})

        result.append({"exchange": exchange, "currency": currency, "price_history": curr_price_history})

    return result


def get_price_history_all_to_one(db: Session, currency: str, start_time: str, end_time: str) -> List[dict]:
    currency_id = db.query(Currency).filter(Currency.code_name == currency).first().id
    # 2014-03-12T13:37:27+00:00 - example start_time
    start_time_dt = datetime.fromisoformat(start_time)
    end_time_dt = datetime.fromisoformat(end_time)
    price_history = (
        db.query(Rate, Exchange)
        .join(Exchange, Rate.market_id == Exchange.id)
        .filter(
            Rate.buy_currency_id == currency_id,
            Rate.time >= start_time_dt,
            Rate.time <= end_time_dt,
        )
        .order_by(Exchange.name)
        .all()
    )

    if len(price_history) > 100:
        step = len(price_history) // 20
        price_history = price_history[::step]

    result = []
    for exchange in set([rate_and_exchange[1].name for rate_and_exchange in price_history]):
        exch_price_history = []
        for rate_and_exchange in price_history:
            if rate_and_exchange[1].name == exchange:
                exch_price_history.append({"date": str(rate_and_exchange[0].time), "price": rate_and_exchange[0].price})

        result.append({"exchange": exchange, "currency": currency, "price_history": exch_price_history})

    return result


def get_price_history_all_to_all(db: Session, start_time: str, end_time: str) -> List[dict]:
    # 2014-03-12T13:37:27+00:00 - example start_time
    start_time_dt = datetime.fromisoformat(start_time)
    end_time_dt = datetime.fromisoformat(end_time)
    price_history = (
        db.query(Rate, Exchange, Currency)
        .join(Exchange, Rate.market_id == Exchange.id)
        .join(Currency, Rate.buy_currency_id == Currency.id)
        .filter(
            Rate.time >= start_time_dt,
            Rate.time <= end_time_dt,
        )
        .order_by(Exchange.name, Currency.code_name)
        .all()
    )

    if len(price_history) > 100:
        step = len(price_history) // 20
        price_history = price_history[::step]

    result = []
    for exchange, currency in sorted(
        set(
            [
                (rate_exchange_currency[1].name, rate_exchange_currency[2].code_name)
                for rate_exchange_currency in price_history
            ]
        )
    ):
        exch_curr_price_history = []
        for rate_exchange_currency in price_history:
            if rate_exchange_currency[1].name == exchange and rate_exchange_currency[2].code_name == currency:
                exch_curr_price_history.append(
                    {"date": str(rate_exchange_currency[0].time), "price": rate_exchange_currency[0].price}
                )

        result.append({"exchange": exchange, "currency": currency, "price_history": exch_curr_price_history})

    return result


if __name__ == "__main__":
    db = get_db().__next__()
    print(get_price_history_all_to_all(db, "2022-05-28 11:42:03.027798", "2022-05-28 11:45:46.500433"))

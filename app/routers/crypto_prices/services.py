from typing import List

import aiohttp
import asyncio

from aiohttp import ContentTypeError
from sqlalchemy.orm import Session

from app.routers.crypto_prices.db.services import (
    get_name_tags_of_currencies,
    insert_new_prices,
    get_price_history_one_to_one,
    get_price_history_one_to_all,
    get_price_history_all_to_one,
    get_price_history_all_to_all,
)
from app.routers.database import get_db


# TODO убрать строку, если без неё пашет
# somehow this setting prevents RuntimeError: Event loop is closed but I don't know why...
# asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


def get_list_of_price_histories(
    db: Session, exchange: str, currency: str, start_time: str, end_time: str
) -> List[dict]:
    # 1 exchange   - 1 currency
    # 1 exchange   - all currency
    # all exchange - 1 currency
    # all exchange - all currency
    if exchange != "all" and currency != "all":
        result = get_price_history_one_to_one(db, exchange, currency, start_time, end_time)
    elif exchange != "all" and currency == "all":
        result = get_price_history_one_to_all(db, exchange, start_time, end_time)
    elif exchange == "all" and currency != "all":
        result = get_price_history_all_to_one(db, currency, start_time, end_time)
    else:
        result = get_price_history_all_to_all(db, start_time, end_time)
    return result


async def cryptocom_get_currency_price(buy_tag: str, sell_tag: str = "USDT") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.crypto.com/v2/public/get-ticker?instrument_name={buy_tag}_{sell_tag}"
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data["result"]["data"]["k"]
            except TypeError:
                return None


async def coinbase_get_currency_price(buy_tag: str, sell_tag: str = "USD") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.coinbase.com/v2/prices/{buy_tag}-{sell_tag}/buy"  # this includes 1% coinbase fee
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data["data"]["amount"]
            except KeyError:
                return None


async def gemini_get_currency_price(buy_tag: str, sell_tag: str = "USD") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.gemini.com/v1/pubticker/{buy_tag}{sell_tag}"
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data["ask"]
            except KeyError:
                return None


async def binance_get_currency_price(buy_tag: str, sell_tag: str = "USDT") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://www.binance.com/api/v3/ticker/price?symbol={buy_tag}{sell_tag}"
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data["price"]
            except KeyError:
                return None


async def kraken_get_currency_price(buy_tag: str, sell_tag: str = "USD") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.kraken.com/0/public/Ticker?pair={buy_tag}{sell_tag}"
        async with session.get(api_url) as response:
            data: dict = await response.json()
            try:
                pair_tag = next(iter(data["result"].keys()))
                return data["result"][pair_tag]["a"][0]
            except (KeyError, StopIteration):
                return None


async def bitmart_get_currency_price(buy_tag: str, sell_tag: str = "USDT") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api-cloud.bitmart.com/spot/v1/ticker?symbol={buy_tag}_{sell_tag}"
        async with session.get(api_url) as response:
            try:
                data: dict = await response.json()
                return data["data"]["tickers"][0]["last_price"]
            except ContentTypeError:
                return None


async def bitfinex_get_currency_price(buy_tag: str, sell_tag: str = "USD") -> dict:
    if buy_tag == "DOGE":
        return None
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api-pub.bitfinex.com/v2/ticker/t{buy_tag}{sell_tag}"
        async with session.get(api_url) as response:
            data = await response.json()
            if data[2] != "symbol: invalid":
                return data[2]
            else:
                return None


async def kucoin_get_currency_price(buy_tag: str, sell_tag: str = "USDT") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.kucoin.com/api/v1/market/orderbook/level1?symbol={buy_tag}-{sell_tag}"
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data["data"]["price"]
            except TypeError:
                return None


async def gateio_get_currency_price(buy_tag: str, sell_tag: str = "USDT") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.gateio.ws/api/v4/spot/tickers?currency_pair={buy_tag}_{sell_tag}"
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data[0]["lowest_ask"]
            except KeyError:
                return None


async def exmo_get_currency_price(buy_tag: str, sell_tag: str = "USD") -> dict:
    async with aiohttp.ClientSession() as session:
        api_url = f"https://api.exmo.com/v1.1/ticker"
        async with session.get(api_url) as response:
            data = await response.json()
            try:
                return data[f"{buy_tag}_{sell_tag}"]["buy_price"]
            except KeyError:
                return None


async def update_currency_prices():
    db = get_db().__next__()
    currency_list = get_name_tags_of_currencies(db)
    currency_price_data = dict()

    currency_price_data["Crypto.com Exchange"] = {}
    currency_price_data["Coinbase Exchange"] = {}
    currency_price_data["Gemini"] = {}
    currency_price_data["Binance"] = {}
    currency_price_data["Kraken"] = {}
    currency_price_data["BitMart"] = {}
    currency_price_data["Bitfinex"] = {}
    currency_price_data["KuCoin"] = {}
    currency_price_data["Gate.io"] = {}
    currency_price_data["EXMO"] = {}
    for curr in currency_list:
        (
            cryptocom_price,
            coinbase_price,
            gemini_price,
            binance_price,
            kraken_price,
            bitmart_price,
            bitfinex_price,
            kucoin_price,
            gateio_price,
            exmo_price,
        ) = await asyncio.gather(
            cryptocom_get_currency_price(buy_tag=curr["code_name"]),
            coinbase_get_currency_price(buy_tag=curr["code_name"]),
            gemini_get_currency_price(buy_tag=curr["code_name"]),
            binance_get_currency_price(buy_tag=curr["code_name"]),
            kraken_get_currency_price(buy_tag=curr["code_name"]),
            bitmart_get_currency_price(buy_tag=curr["code_name"]),
            bitfinex_get_currency_price(buy_tag=curr["code_name"]),
            kucoin_get_currency_price(buy_tag=curr["code_name"]),
            gateio_get_currency_price(buy_tag=curr["code_name"]),
            exmo_get_currency_price(buy_tag=curr["code_name"]),
        )

        currency_price_data["Crypto.com Exchange"][curr["id"]] = cryptocom_price
        currency_price_data["Coinbase Exchange"][curr["id"]] = coinbase_price
        currency_price_data["Gemini"][curr["id"]] = gemini_price
        currency_price_data["Binance"][curr["id"]] = binance_price
        currency_price_data["Kraken"][curr["id"]] = kraken_price
        currency_price_data["BitMart"][curr["id"]] = bitmart_price
        currency_price_data["Bitfinex"][curr["id"]] = bitfinex_price
        currency_price_data["KuCoin"][curr["id"]] = kucoin_price
        currency_price_data["Gate.io"][curr["id"]] = gateio_price
        currency_price_data["EXMO"][curr["id"]] = exmo_price

    print(currency_price_data)
    insert_new_prices(db, currency_price_data)


# Testing
if __name__ == "__main__":
    asyncio.run(update_currency_prices())

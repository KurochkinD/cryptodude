import os
import re
from typing import List, Union

from fastapi import APIRouter, Depends, UploadFile, status, HTTPException
from fastapi.openapi.models import Response
from sqlalchemy.orm import Session

from app.routers.auth.schemas import User as ResponseUser
from app.routers.database import get_db
from app.routers.user_api.schemas import CryptoSub, ExchangeSub, PasswordChange
from app.routers.user_api.services import (
    query_user_crypto_subscriptions,
    query_user_exchange_subscriptions,
    query_user_info,
    save_photo,
    query_user_has_exchange_subscription,
    query_user_has_crypto_subscription,
    query_user_subscribed_to_exchange,
    query_user_subscribed_to_currency,
)
from app.routers.auth.db.models import User
from app.routers.auth.db.services import verify_password, get_password_hash

from app.routers.auth.services import get_current_user
from app.routers.constants import STORAGE_PATH


users_app = APIRouter(prefix="/users", tags=["Users"])


@users_app.get("/me/crypto_subscriptions", response_model=List[CryptoSub])
async def get_user_crypto_subscriptions(current_user: User = Depends(get_current_user), db: Session = Depends(get_db)):
    """
    This endpoint will return all subscriptions to cryptocurrency of user with id :user_id
    """
    result = query_user_crypto_subscriptions(db, current_user.id)
    return result


@users_app.get("/me/exchange_subscriptions", response_model=List[ExchangeSub])
async def get_user_exchange_subscriptions(
    current_user: User = Depends(get_current_user), db: Session = Depends(get_db)
):
    """
    This endpoint will return all subscriptions to exchange of user with id :user_id
    """
    result = query_user_exchange_subscriptions(db, current_user.id)
    return result


@users_app.get("/me/has_exchange_subscription", response_model=bool)
async def has_exchange_subscription(current_user: User = Depends(get_current_user), db: Session = Depends(get_db)):
    """
    This endpoint will return true if user has exchange subscription, else false
    """
    result = query_user_has_exchange_subscription(db, current_user.id)
    return result


@users_app.get("/me/has_crypto_subscription", response_model=bool)
async def has_crypto_subscription(current_user: User = Depends(get_current_user), db: Session = Depends(get_db)):
    """
    This endpoint will return true if user has crypto subscription, else false
    """
    result = query_user_has_crypto_subscription(db, current_user.id)
    return result


@users_app.get("/me/subscribed_to_exchange/{exchange_id}", response_model=bool)
async def user_subscribed_to_exchange(
    exchange_id: int, current_user: User = Depends(get_current_user), db: Session = Depends(get_db)
):
    """
    This endpoint will return all information of user who requested it
    """
    result = query_user_subscribed_to_exchange(db, current_user.id, exchange_id)
    return result


@users_app.get("/me/subscribed_to_currency/{currency_id}", response_model=bool)
async def user_subscribed_to_currency(
    currency_id: int, current_user: User = Depends(get_current_user), db: Session = Depends(get_db)
):
    """
    This endpoint will return all information of user who requested it
    """
    result = query_user_subscribed_to_currency(db, current_user.id, currency_id)
    return result


@users_app.get("/me", response_model=ResponseUser)
async def get_user_profile_info(current_user: User = Depends(get_current_user), db: Session = Depends(get_db)):
    """
    This endpoint will return all information of user who requested it
    """
    result = query_user_info(db, current_user.id)
    return result


@users_app.post("/me/edit", status_code=status.HTTP_200_OK)
async def edit_profile(
    name: str,
    photo: Union[UploadFile, None] = None,
    current_user: User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """
    This endpoint will edit profile of user who requested it
    :param name: name of user
    :param photo: avatar of user

    :return: response with status code
    """
    if photo is not None:
        photo_folder = os.path.join("users", f"{current_user.id}")
        photo_filename = f"avatar.{photo.filename.rsplit('.', maxsplit=1)[1]}"
        photo_path = os.path.join(photo_folder, photo_filename)
        photo_folder_abs_path = os.path.abspath(os.path.join(STORAGE_PATH, photo_folder))
        photo_abs_path = os.path.abspath(os.path.join(STORAGE_PATH, photo_path))
        if not os.path.exists(photo_folder_abs_path):
            os.makedirs(photo_folder_abs_path)
        saved, error = await save_photo(photo, photo_abs_path)
        if saved:
            current_user.photo = photo_path
        else:
            raise error
    if name != current_user.name and isinstance(name, str) and len(name) > 0:
        current_user.name = name
    db.commit()
    return Response(status_code=status.HTTP_200_OK, description="Success")


@users_app.post("/me/change_password", status_code=status.HTTP_200_OK)
async def change_password(
    password: PasswordChange,
    current_user: User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """
    This endpoint will change password of user who requested it
    :param password: old and new passwords
    """
    if not verify_password(password.old_password, current_user.password):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Wrong password")
    elif not re.fullmatch(r'[A-Za-z0-9!@#$%^&*()_\-+={\[}\]|\\:;"\'<,>.?]{8,}', password.new_password):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="New password should contain uppercase, lowercase letters, "
            "numbers and special symbols and be at least 8 charachters long",
        )

    else:
        current_user.password = get_password_hash(password.new_password)
        db.commit()
        return Response(status_code=status.HTTP_200_OK, description="Success")

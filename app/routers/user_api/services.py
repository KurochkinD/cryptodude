import os

import aiofiles
from fastapi import UploadFile
from sqlalchemy.orm import Session, load_only

from app.routers.auth.db.models import User
from app.routers.cryptocurrencies.db.models import CurrencySub, Currency
from app.routers.exchanges.db.models import Exchange, ExchangeSub


def query_user_crypto_subscriptions(db: Session, user_id: int):
    # join CurrencySub with Currency and filter with user_id
    records = (
        db.query(Currency)
        .join(CurrencySub, CurrencySub.currency_id == Currency.id)
        .filter(CurrencySub.user_id == user_id)
        .all()
    )

    return [{"name": curr.name, "logo": curr.logo} for curr in records]


def query_user_exchange_subscriptions(db: Session, user_id: int):
    # join CurrencySub with Currency and filter with user_id
    records = (
        db.query(Exchange)
        .join(ExchangeSub, ExchangeSub.exchange_id == Exchange.id)
        .filter(ExchangeSub.user_id == user_id)
        .all()
    )

    return [{"name": exchange.name, "url": exchange.url, "logo": exchange.logo} for exchange in records]


def query_user_has_exchange_subscription(db: Session, user_id: int):
    record = db.query(ExchangeSub).filter(ExchangeSub.user_id == user_id).first()
    return bool(record)


def query_user_has_crypto_subscription(db: Session, user_id: int):
    record = db.query(CurrencySub).filter(CurrencySub.user_id == user_id).first()
    return bool(record)


def query_user_subscribed_to_exchange(db: Session, user_id: int, exchange_id: int):
    record = (
        db.query(ExchangeSub).filter(ExchangeSub.user_id == user_id, ExchangeSub.exchange_id == exchange_id).first()
    )
    return bool(record)


def query_user_subscribed_to_currency(db: Session, user_id: int, currency_id: int):
    record = (
        db.query(CurrencySub).filter(CurrencySub.user_id == user_id, CurrencySub.currency_id == currency_id).first()
    )
    return bool(record)


def query_user_info(db: Session, user_id: int):
    return (
        db.query(User).options(load_only("name", "email", "photo", "register_time")).filter(User.id == user_id).first()
    )


async def save_photo(photo: UploadFile, photo_abs_path: str):
    try:
        async with aiofiles.open(photo_abs_path, "wb") as out_file:
            content = await photo.read()
            await out_file.write(content)
    except Exception as e:
        return False, e
    else:
        if not os.path.exists(photo_abs_path):
            return False, Exception("smth went wrong")
        return True, None

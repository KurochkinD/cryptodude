from pydantic import BaseModel, Field


class CryptoSub(BaseModel):
    name: str
    logo: str = None


class ExchangeSub(BaseModel):
    name: str
    url: str
    logo: str = None


class PasswordChange(BaseModel):
    old_password: str
    new_password: str

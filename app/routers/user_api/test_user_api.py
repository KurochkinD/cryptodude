import base64

from fastapi.testclient import TestClient

from app.routers.auth.db.models import User
from app.routers.cryptocurrencies.db.models import CurrencySub
from app.routers.database import DATABASE_URL, Base, get_db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.main import app
from app.routers.exchanges.db.models import ExchangeSub

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_get_crypto_subs():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id
    client.post("/crypto/1/subscribe", json={"user_id": user_id})

    res = client.request(
        "get",
        "/users/me/crypto_subscriptions",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert res.status_code == 200

    db.query(CurrencySub).filter(CurrencySub.currency_id == 1, CurrencySub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_get_exchange_subs():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id
    client.post("/exchanges/270/subscribe", json={"user_id": user_id})

    res = client.request(
        "get",
        "/users/me/exchange_subscriptions",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert len(res.json()) > 0

    db.query(ExchangeSub).filter(ExchangeSub.exchange_id == 270, ExchangeSub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_has_exch_sub():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id
    client.post("/exchanges/270/subscribe", json={"user_id": user_id})

    res = client.request(
        "get",
        "/users/me/has_exchange_subscription",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert res.status_code == 200

    db.query(ExchangeSub).filter(ExchangeSub.exchange_id == 270, ExchangeSub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_has_crypto_sub():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id
    client.post("/crypto/1/subscribe", json={"user_id": user_id})

    res = client.request(
        "get",
        "/users/me/has_crypto_subscription",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert res.status_code == 200

    db.query(CurrencySub).filter(CurrencySub.currency_id == 1, CurrencySub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_subbed_to_exch():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id
    client.post("/exchanges/270/subscribe", json={"user_id": user_id})

    res = client.request(
        "get",
        "users/me/subscribed_to_exchange/270",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert res.status_code == 200

    db.query(ExchangeSub).filter(ExchangeSub.exchange_id == 270, ExchangeSub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_subbed_to_crypto():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    db = override_get_db().__next__()
    user_id = db.query(User).filter(User.email == "abobus@mail.ru").first().id
    client.post("/crypto/1/subscribe", json={"user_id": user_id})

    res = client.request(
        "get",
        "users/me/subscribed_to_currency/1",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert res.status_code == 200

    db.query(CurrencySub).filter(CurrencySub.currency_id == 1, CurrencySub.user_id == user_id).delete()
    db.commit()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_profile_settings():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    with open("D:\Program Files D\ф\остальное\\48TjOaqsn4s.jpg", "rb") as _photo:
        res = client.post(
            "/users/me/edit?name=abobus",
            headers={"Authorization": f"Bearer {token}"},
            files={"photo": ("@48TjOaqsn4s.jpg", _photo, "image/jpeg")},
        )
        assert res.status_code == 200

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_change_password():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    res = client.post(
        "/users/me/change_password",
        headers={
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json",
        },
        json={"old_password": "Chimichangas4life!", "new_password": "Chimichangas4life!"},
    )
    assert res.status_code == 200

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_change_password_not_match():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    res = client.post(
        "/users/me/change_password",
        headers={"Authorization": f"Bearer {token}"},
        json={"old_password": "Chimichangas4lif", "new_password": "Chimichangas4life!"},
    )
    assert res.status_code == 400

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()


def test_user_change_password_bad_new_password():
    client.post(
        "/auth/register",
        json={"name": "abobus", "email": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    response = client.post(
        "/auth/token",
        data={"grant_type": "password", "username": "abobus@mail.ru", "password": "Chimichangas4life!"},
    )
    token = response.json()["access_token"]

    res = client.post(
        "/users/me/change_password",
        headers={"Authorization": f"Bearer {token}"},
        json={"old_password": "Chimichangas4life!", "new_password": "ogdan"},
    )
    assert res.status_code == 400

    db = override_get_db().__next__()
    db.query(User).filter(User.email == "abobus@mail.ru").delete()
    db.commit()

import os

from environs import Env

env = Env()
env.read_env()

SECRET_KEY = env.str("SECRET_KEY")
ALGORITHM = env.str("ALGORITHM")
ACCESS_TOKEN_EXPIRE_MINUTES = env.int("ACCESS_TOKEN_EXPIRE_MINUTES")

ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
STORAGE_PATH = os.path.abspath(os.path.join(ROOT_PATH, "storage"))

# DB SETTINGS
DB_USER = env.str("DB_USER")
DB_PASSWORD = env.str("DB_PASSWORD")
DB_SERVER = env.str("DB_SERVER")
DB_NAME = env.str("DB_NAME")

# Google OAuth settings
CLIENT_ID = env.str("CLIENT_ID")

# Coinmarketcap key
COINMARKETCAP_KEY = env.str("COINMARKETCAP_KEY")
EXCHANGES_LIST = ["Binance", "Coinbase Exchange", "Crypto.com Exchange", "Gemini", "BitMart", "Kraken",
                  "Bitfinex", "KuCoin", "Gate.io", "EXMO"]
AMOUNT_OF_CRYPTOCURRENCIES = 30
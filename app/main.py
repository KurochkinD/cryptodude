import asyncio

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.sessions import SessionMiddleware
from fastapi.staticfiles import StaticFiles

from app.routers.auth.endpoints import auth_app
from app.routers.constants import SECRET_KEY
from app.routers.crypto_prices.endpoints import crypto_api
from app.routers.cryptocurrencies.endpoints import crypto_app
from app.routers.exchanges.endpoints import exchanges_app
from app.routers.faq.endpoints import faq_app
from app.routers.user_api.endpoints import users_app

from .routers.constants import STORAGE_PATH
from .routers.crypto_prices.services import update_currency_prices
from .routers.cryptocurrencies.services import update_cryptocurrencies
from .routers.exchanges.services import update_commissions, update_exchange_list

app = FastAPI()

app.mount("/storage", StaticFiles(directory=STORAGE_PATH), name="storage")

app.include_router(auth_app)
app.include_router(crypto_app)
app.include_router(users_app)
app.include_router(faq_app)
app.include_router(crypto_api)
app.include_router(exchanges_app)

app.add_middleware(SessionMiddleware, secret_key=SECRET_KEY)
origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000",
    "http://localhost:3001",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": "Hello Bigger Applications!"}


if __name__ == "__main__":
    asyncio.run(update_exchange_list())
    asyncio.run(update_cryptocurrencies())
    asyncio.run(update_commissions())
    asyncio.run(update_currency_prices())
    asyncio.run(update_currency_prices())

    uvicorn.run(app, host="0.0.0.0", port=8000)
